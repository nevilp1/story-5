from django.shortcuts import render, redirect	
from django.http import HttpResponse
from .forms import JadwalForm
from .models import Jadwal
from django.utils import timezone
import datetime

def addForm(request):
    form = JadwalForm()
    if request.method =="POST":
        form=JadwalForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('jadwalkegiatan:jadwal')
    return render(request, "addform.html", {'form':form})

def jadwal(request):
    # print(dir(request.body))
    jadwals = Jadwal.objects.order_by("tanggal", "waktu")

    response = {
        'jadwal' :jadwals,
        'server_time' : timezone.now(),
    }
    return render(request, "formpage.html", response)
# Create your views here.

def delete(request):
    if request.method == "POST":
        id = request.POST['id']
        Jadwal.objects.get(id=id).delete()
    return redirect('jadwalkegiatan:jadwal')
