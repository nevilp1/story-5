from . import models
from django import forms

class JadwalForm(forms.ModelForm):
        nama_kegiatan = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "jadwalfields full",
                "required" : True,
                "placeholder":"Nama Kegiatan",
                }))
        tempat = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "jadwalfields full",
                "required" : True,
                "placeholder":"Tempat",
                }))
        prioritas = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "jadwalfields full",
                "required" : True,
                "placeholder":"Prioritas",
                }))
        tanggal = forms.DateField(widget=forms.SelectDateWidget(attrs={
                "class" : "datefield jadwalfields",
                "required" : True,
                }))
        waktu = forms.TimeField(widget=forms.TimeInput(attrs={
                "class" : "datefield jadwalfields",
                "placeholder":"00:00",
                "required" : True,
                }))


        class Meta:
                model = models.Jadwal
                fields = ["nama_kegiatan", "prioritas", "tempat", "tanggal","waktu"]        