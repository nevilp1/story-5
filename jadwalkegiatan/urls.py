from django.urls import path
from .import views

app_name = 'jadwalkegiatan'

urlpatterns = [
 	path('add', views.addForm, name='addForm'),
 	path('',  views.jadwal, name='jadwal'),
 	path('delete', views.delete)
]