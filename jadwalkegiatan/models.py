from django.db import models
import datetime
from django.utils import timezone

class Jadwal(models.Model):
    nama_kegiatan = models.CharField(max_length=50)
    tempat = models.CharField(max_length=25)
    prioritas = models.CharField(null=True, max_length=25)
    tanggal = models.DateField(default=timezone.now)
    waktu = models.TimeField(default=timezone.now)

    def __str__(self):
        return self.nama_kegiatan

# Create your models here.
